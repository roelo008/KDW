import datetime
import os
import geopandas as gp
import numpy as np
import pandas as pd

from brondata import KDW_VARIANTEN, OUT_DIR, KDW_TABEL_SOURCE, AERIUS_HEX_NDEP_SOURCE, AERIUS_HEX_HABTYPE_SOURCE
from build_hexagons_complete import hexagons_complete

SAMPLE = False

overschrijding_labels = ['ha_kleiner_KDW', 'ha_gelijk_groter_KDW']
hexagons = hexagons_complete(sample=SAMPLE)

ts = datetime.datetime.now().strftime("%Y%m%d-%H%M")
outname = f'{os.path.splitext(os.path.basename(KDW_TABEL_SOURCE))[0]}_uitgebreide_tabel_{ts}.xlsx'
output = os.path.join(OUT_DIR, outname)

with pd.ExcelWriter(output, mode='w') as dest:

    # Metadata sheet
    pd.Series({'content': 'KDW overschrijding per habitattype voor 3 KDW varianten',
                'created by': os.environ.get('username'),
               'date': ts,
               'source_kdws': KDW_TABEL_SOURCE,
               'source_total_n-depositie': AERIUS_HEX_NDEP_SOURCE,
               'source_hexagon_habitattype': AERIUS_HEX_HABTYPE_SOURCE
               }).to_excel(dest, sheet_name='metadata')

    # Alle Hexagonen
    hexagons.drop(columns=['geometry']).sort_values(by=['habitat_t0', 'receptor_i', 'ht_area_ha']).to_excel(dest, sheet_name='alle_hexagonen',
                                                 index=False, freeze_panes=(1, 0))

    for kdw in KDW_VARIANTEN:

        hex_selection = gp.GeoDataFrame(
            hexagons.loc[hexagons[kdw].notna(), :].copy().astype({kdw: float})
        )

        hex_selection['gte_kdw'] = np.where(hex_selection.total_depo >= hex_selection[kdw],
                                            overschrijding_labels[1], overschrijding_labels[0])

        out = pd.pivot_table(hex_selection, index='habitat_t0',
                       columns='gte_kdw', values='ht_area_ha', aggfunc='sum').fillna(0)
        out[kdw] = out.index.map(hexagons.drop_duplicates(subset=['habitat_t0', kdw]).set_index('habitat_t0').loc[:, kdw].to_dict())
        out['area_ha'] = hex_selection.groupby('habitat_t0').ht_area_ha.sum()

        out.loc[:, [kdw, 'area_ha'] + overschrijding_labels].to_excel(dest, sheet_name=kdw,
                                                                      freeze_panes=(1, 1))

