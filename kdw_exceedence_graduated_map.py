import datetime
import geopandas as gp
import numpy as np


from brondata import KDW_VARIANTEN, KDW_TABEL_SOURCE, OUT_DIR
from build_hexagons_complete import hexagons_complete

# Settings
SAMPLE = False
kdw = KDW_VARIANTEN[2]
OUT_GPKG = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\a_projects\KDW2023\output_20240219\kdw_gradual_exceedance.gpkg"
OUT_LAYER = f'{kdw}_exceedance_ratio_{datetime.datetime.now().strftime("%Y%m%d%H%M")}'
EXCEENDANCE_LIMITS = [1, 1.5, 2, 2.5]
EXCEEDANCE_CLASSES = {
    0: "a ratio ≤ 1",
    1: "b 1 < ratio ≤ 1.5",
    2: "c 1.5 < ratio ≤ 2",
    3: "d 2 < ratio ≤ 2.5",
    4: "e ratio > 2.5",
}
EXCEENDANCE_RATIO_COLUMN_NAME = f'ER_{kdw}'

# select non-empty KDWs
hexagons = hexagons_complete(sample=SAMPLE)
hex_selection = gp.GeoDataFrame(
    hexagons.loc[hexagons[kdw].notna(), :].copy().astype({kdw: float})
)

# KDW exceedence per habitattype in each hexagon
hex_selection[EXCEENDANCE_RATIO_COLUMN_NAME] = hex_selection.total_depo.divide(hex_selection[kdw])

# Group by hexagon ID and add summary data
grouped = hex_selection.groupby("receptor_i")
hex = gp.GeoDataFrame(data={}, index=hex_selection.receptor_i.unique())
hex['n_habtypes'] = grouped.ht_area_ha.count()
hex['sum_habtype_area_ha'] = grouped.ht_area_ha.sum()
hex['N_depo'] = grouped.total_depo.mean()
hex['min_kdw'] = grouped[kdw].min()
hex['max_kdw'] = grouped[kdw].max()
hex['mean_kdw'] = grouped[kdw].mean()
hex[f'min_{EXCEENDANCE_RATIO_COLUMN_NAME}'] = grouped[EXCEENDANCE_RATIO_COLUMN_NAME].min()
hex[f'max_{EXCEENDANCE_RATIO_COLUMN_NAME}'] = grouped[EXCEENDANCE_RATIO_COLUMN_NAME].max()
hex[f'mean_{EXCEENDANCE_RATIO_COLUMN_NAME}'] = grouped[EXCEENDANCE_RATIO_COLUMN_NAME].mean()

# Classify exceedance
for stat in ['min', 'max', 'mean']:
    hex[f"{stat}_{EXCEENDANCE_RATIO_COLUMN_NAME}_class"] = [
    EXCEEDANCE_CLASSES[x]
    for x in np.digitize(hex[f"{stat}_{EXCEENDANCE_RATIO_COLUMN_NAME}"], EXCEENDANCE_LIMITS, right=True)
]

# Write to file
hex.set_geometry(
    hex_selection.drop_duplicates(subset=["receptor_i"])
    .set_index("receptor_i")
    .geometry,
    inplace=True,
)
hex.to_file(OUT_GPKG, layer=OUT_LAYER, encodings="utf-8")
