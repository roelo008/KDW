# KDW update 2023

## About
Deze repository verzorgt de kartering en figuren behorende bij van Dobben et al (2024). 

![demo map](resources/demo_map.png)

## Contact
Hans Roelofsen [https://www.wur.nl/nl/en/personen/hans-roelofsen.htm]

## Status
In press

