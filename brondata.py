# SOURCEDATA

# KDW Tabel, door Han & Wieger
KDW_TABEL_SOURCE = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\a_projects\KDW2023\KDWtabelVoorArtikel.xlsx"
KDW_TABEL_SHEET = r"BerekenKDW2022"

# Aerius hexagonen met informatie over habitattype
# AERIUS_HEX_HABTYPE_SOURCE = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\hexagons_to_relevant_habitats\hexagons_to_relevant_habitatsPolygon.shp"
AERIUS_HEX_HABTYPE_SOURCE = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\RIVM\hexagons_to_relevant_habitats\hexagons_to_relevant_habitatsPolygon.shp"
# AERIUS_HEX_HABTYPE_DOWNLOAD: https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/bf6fb96b-16ea-4f30-9ac9-d66a18f674ad

# Aerius hexagonen met totale N-depositie
# AERIUS_HEX_NDEP_SOURCE = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\AuriusTotaleStikstofdepositie\depositionsPolygon.shp"
AERIUS_HEX_NDEP_SOURCE = r"c:\Users\roelo008\OneDrive - Wageningen University & Research\b_geodata\RIVM\AuriusTotaleStikstofdepositie\depositionsPolygon.shp"
# AERIUS_HEX_NDEP_DOWNLOAD: https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/b5e7a6f3-aa6d-483b-bebc-c5f7ddd9a233

KDW_VARIANTEN = ["KDW2012", "KDW2022Rapport", "KDW2022Artikel"]  # kg/ha/jaar
KDW_UNIT_ORIGINAL = 'kg_ha_jr'
KDW_UNIT_WORKING = 'mol_ha_jr'
MOLGEWICHT_N = 0.014  # 1 mol == 0.014 kg
HABITATTYPE_AREAAL_KOLOM = "ht_area_ha"

OUT_DIR = r'c:\Users\roelo008\OneDrive - Wageningen University & Research\a_projects\KDW2023\output_20231106'
