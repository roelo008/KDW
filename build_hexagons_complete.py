import geopandas as gp
import pandas as pd
import sys

from brondata import (
    KDW_TABEL_SOURCE,
    KDW_TABEL_SHEET,
    AERIUS_HEX_HABTYPE_SOURCE,
    AERIUS_HEX_NDEP_SOURCE,
    KDW_VARIANTEN,
    MOLGEWICHT_N,
    HABITATTYPE_AREAAL_KOLOM,
    KDW_UNIT_ORIGINAL,
    KDW_UNIT_WORKING
)


def aerius_hexagonen(sample: bool=False) -> gp.GeoDataFrame:


    # Read hexagons with Habitattype info
    hexagons = gp.read_file(AERIUS_HEX_HABTYPE_SOURCE).drop(
        columns=["zoom_level", "natura2000", "habitat_ty", "critical_d"]
    )

    if sample:
        hexagons = hexagons.sample(500)

    # Calculate area of habitattype per hexagon
    hexagons[HABITATTYPE_AREAAL_KOLOM] = hexagons.surface.multiply(
        hexagons.coverage
    ).divide(
        10000
    )  # dit nog navragen bij RIVM

    return hexagons


def aerius_hexagons_ndep() -> pd.DataFrame:
    """

    :return:
    """

    return gp.read_file(AERIUS_HEX_NDEP_SOURCE, ignore_geometry=True).query("zoom_level == 1").drop(columns=["zoom_level", "year"])


def kdw_tabel() -> pd.DataFrame:
    # Read KDW tabel

    original_column_names = [f'{kdw}_{KDW_UNIT_ORIGINAL}' for kdw in KDW_VARIANTEN]
    working_column_names = KDW_VARIANTEN

    try:
        kdw_tab = (
            pd.read_excel(
                KDW_TABEL_SOURCE,
                sheet_name=KDW_TABEL_SHEET,
                usecols=[0, 6, 7, 8],
                names=["Code"] + original_column_names,
            )
            .dropna(subset=original_column_names, how="all")
            .set_index("Code", verify_integrity=True)
        )
    except PermissionError as e:
        print(e)
        sys.exit(0)

    kdw_tab.drop(
        labels=kdw_tab.loc[kdw_tab.index.str.startswith("Lg")].index, inplace=True
    )

    # KDW eenheid conversie naar mol/ha/jaar
    kdw_tab[working_column_names] = kdw_tab[original_column_names].divide(
        MOLGEWICHT_N
    )

    return kdw_tab


def hexagons_complete(sample=False) -> pd.DataFrame:
    """
    Return hexagon GeoDataFrame With attributes:

      <source: aerius_hex_habtypen_src>
        receptor_i:       six-digit ID number of hexagon. Non-unique
        natura2001:       Naam Natura2000 gebied
        habitat_t0:       Habitattype codering
        habitat_t1:       Habitatttype omschrijving
        surface:          oppervlakte van 't habitattype binnen hexagon in m2
        coverage:         bedekking van zelfde, schaal [0-1]
        ht_area_ha:       effectief oppervlak van habitattype binnen hexagon (surface X coverage). Berekend!
      <source: kdw_tabel_src>
        KDW2012:          Kritische Depositiewaarde variant 01, zoas in 2023
        KDW2022Rapport:   Kritische Depositiewaarde variant 02, zoals in rapport 2022
        KDW2022Artikel:   Kritische Depositiewaarde variant 03, zoals in artikel 2023
        (KDWs oorspronkelijk in kg/ha/jr, wordt omgerekend naar mol/ha/jaar)
      <source: aerius_hex_ndep_src>
        total_depo:       totale N-depositie in mol/ha/jr


    Returns inner-join relation between <aerius_hex_habitattypen_src.habitat_t0> en <kdw_tab_src.habitattype>

    :return:
    """

    # Get sourcedata
    kdw_tab = kdw_tabel()
    hexagons_habtype = aerius_hexagonen(sample=sample)
    hexagons_ndep = aerius_hexagons_ndep()

    # Join KDW in 3 varianten to hexagons with habtype
    hexagons_habtype = hexagons_habtype.join(
        kdw_tab, how="inner", on="habitat_t0", validate="many_to_one"
    )

    s1 = set(hexagons_habtype.habitat_t0)
    if len(kdw_tab.index.difference(s1)) > 0:
        print(f'!Warning! Habtypen with KDW but not found in Hexagons: {", ".join([i for i in list(kdw_tab.index.difference(s1))])}')

    # Join actual N-deposition data to hexagons with habtype
    hexagons = pd.merge(
        left=hexagons_habtype,
        right=hexagons_ndep,
        left_on="receptor_i",
        right_on="receptor_i",
        how="left",
        validate="many_to_one",
    )
    return hexagons
