import datetime
import pandas as pd
import geopandas as gp
import numpy as np

from brondata import KDW_VARIANTEN, KDW_TABEL_SOURCE, OUT_DIR
from build_hexagons_complete import hexagons_complete

# Settings
SAMPLE = False
kdw = KDW_VARIANTEN[2]

# select non-empty KDWs
hexagons = hexagons_complete(sample=SAMPLE)
hex_selection = gp.GeoDataFrame(
    hexagons.loc[hexagons[kdw].notna(), :].copy().astype({kdw: float})
)

# KDW exceedence per habitattype in each hexagon
hex_selection[f'{kdw}_exceedance'] = [{1: f'above_kdw',
                                       0: f'below_kdw'}[i] for i in
                                      np.where(hex_selection.total_depo >= hex_selection[kdw], 1, 0)]

# Group by hexagon ID and add summary data
grouped = hex_selection.groupby(f"{kdw}_exceedance").ht_area_ha.sum()

# Summary
summary = pd.Series({
    'n_hexagonen': 470025,
    f'n_hexagonen_met_{kdw}': hex_selection.shape[0],
    'total_habtype_ha': hex_selection.ht_area_ha.sum(),
    'habtype_depo_<_kdw_ha': grouped.below_kdw,
    'habtype_depo_>=_kdw_ha': grouped.above_kdw,
    'habtype_depo_<_kdw_perc': np.multiply(np.divide(grouped.below_kdw, hex_selection.ht_area_ha.sum()), 100),
    'habtype_depo_>=_kdw_perc': np.multiply(np.divide(grouped.above_kdw, hex_selection.ht_area_ha.sum()), 100),
})
