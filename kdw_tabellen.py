import datetime
import os
import geopandas as gp
import numpy as np
import pandas as pd

from brondata import KDW_VARIANTEN, OUT_DIR, KDW_TABEL_SOURCE
from build_hexagons_complete import hexagons_complete

SAMPLE = False

hexagons = hexagons_complete(sample=SAMPLE)

limits = [1, 1.5, 2, 3]
labels = [
    "kleiner dan KDW",
    "1 - 1.5 X KDW",
    "1.5 - 2 X KDW",
    "2 - 3 X KDW",
    ">3 X KDW",
]
d = dict(zip(range(len(limits) + 1), labels))

out = pd.DataFrame(columns=KDW_VARIANTEN, index=labels)

kdw = KDW_VARIANTEN[0]

for kdw in KDW_VARIANTEN:

    # select non-empty KDWs
    hex_selection = gp.GeoDataFrame(
        hexagons.loc[hexagons[kdw].notna(), :].copy().astype({kdw: float})
    )

    # Verhouding tussen depositie en KDW
    hex_selection["overschrijding"] = hex_selection.total_depo.divide(getattr(hex_selection, kdw))

    # Digitize
    hex_selection["kwalificatie"] = np.digitize(hex_selection.overschrijding, limits)

    # Indicator
    hex_selection["indicator"] = hex_selection.kwalificatie.map(d)

    hex_selection.drop(columns=['geometry']).to_clipboard(excel=True, index=False, sep=',')

    # Sommer areaal naar indicator
    out[kdw] = hex_selection.groupby("indicator").ht_area_ha.sum()


out.to_clipboard()
outname = f'{os.path.splitext(os.path.basename(KDW_TABEL_SOURCE))[0]}_overschrijdingstabel_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}.csv'
out.to_csv(os.path.join(OUT_DIR, outname))
