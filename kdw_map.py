import os
import datetime
import geopandas as gp
import numpy as np


from brondata import KDW_VARIANTEN, KDW_TABEL_SOURCE, OUT_DIR
from build_hexagons_complete import hexagons_complete

SAMPLE = False

hexagons = hexagons_complete(sample=SAMPLE)

for kdw in KDW_VARIANTEN:

    # select non-empty KDWs
    hex_selection = gp.GeoDataFrame(
        hexagons.loc[hexagons[kdw].notna(), :].copy().astype({kdw: float})
    )

    # Keep hexagons with lowest KDW. See https://stackoverflow.com/a/54471056
    hex_min_kdw = hex_selection.loc[getattr(hex_selection.groupby("receptor_i"), kdw).idxmin()].set_index(
        "receptor_i", verify_integrity=True
    )

    # Add counter for number of habitattypen in hexagon
    hex_min_kdw["n_ht"] = hex_selection.groupby("receptor_i").habitat_t0.count()

    # Bepaal overschrijding van KDW
    hex_min_kdw["gte_kdw"] = np.where(hex_min_kdw.total_depo >= hex_min_kdw[kdw], "Ja", "Nee")

    outname = f'{os.path.splitext(os.path.basename(KDW_TABEL_SOURCE))[0]}_{kdw}_overschrijdingshexagons_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}.shp'
    hex_min_kdw.to_file(os.path.join(OUT_DIR, outname))

